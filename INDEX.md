# NLSFunc

NLSFUNC adds NLS (National Language Support) functionality


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## NLSFUNC.LSM

<table>
<tr><td>title</td><td>NLSFunc</td></tr>
<tr><td>version</td><td>0.4a</td></tr>
<tr><td>entered&nbsp;date</td><td>2006-08-22</td></tr>
<tr><td>description</td><td>NLSFUNC adds NLS (National Language Support) functionality</td></tr>
<tr><td>keywords</td><td>freedos, nls, country, display, mode</td></tr>
<tr><td>author</td><td>Eduardo Casino-Almao &lt;mail@eduardocasino.es&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eduardo Casino-Almao &lt;mail@eduardocasino.es&gt;</td></tr>
<tr><td>platforms</td><td>DOS (nasm)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
